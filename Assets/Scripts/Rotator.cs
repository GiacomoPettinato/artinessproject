using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArtinessProject
{
    public class Rotator : MonoBehaviour
    {
        public enum State { AutoRotation, ManualRotation, Waiting }

        [SerializeField, ReadOnly] State state;

        [Header("Manual Rotation")]
        [SerializeField] float dragSensitivity = 0.1f;
        Vector3 mouseReferencePos;
        Vector3 mouseOffset;
        Vector3 inputAngles = Vector3.zero;

        [Header("Auto Rotation")]
        [SerializeField] Vector3 autoRotationAngles = Vector3.up;
        [SerializeField] float autoRotationSpeed = 20.0f;

        [Header("Waiting")]
        public float waitTime = 4.0f;
        float elapsedTime;

        void SetState(State newState)
        {
            if (newState == state)
                return;

            switch (newState)
            {
                case State.ManualRotation:                    
                    mouseReferencePos = Input.mousePosition;
                    break;

                case State.Waiting:
                    elapsedTime = 0.0f;
                    break;

            }
            
            state = newState;
        }

        void AutoRotate()
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(transform.eulerAngles + autoRotationAngles), autoRotationSpeed * Time.deltaTime);
        }

        void ManualRotate()
        {
            if (!Input.GetMouseButton(0))
            {
                SetState(State.Waiting);
                return;
            }

            mouseOffset = (Input.mousePosition - mouseReferencePos);
            inputAngles.y = -(mouseOffset.x + mouseOffset.y) * dragSensitivity;
            transform.Rotate(inputAngles);
            mouseReferencePos = Input.mousePosition;
        }

        void Wait()
        {
            elapsedTime += Time.deltaTime;

            if (elapsedTime >= waitTime)
            {
                SetState(State.AutoRotation);
            }
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0) && !UIManager.Instance.UserIsInspectingUI() && !UIManager.Instance.UserIsInteractingWithUI())
            {
                SetState(State.ManualRotation);
            }

            switch (state)
            {
                case State.AutoRotation:
                    AutoRotate();
                    break;

                case State.ManualRotation:
                    ManualRotate();
                    break;

                case State.Waiting:
                    Wait();
                    break;

            }
        }

    }
}