using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ArtinessProject
{
    public class TimelineSlider : SliderInt
    {
        [SerializeField] TextMeshProUGUI frameText;

        protected override IEnumerator Start()
        {
            yield return base.Start();
            PrintFrameText();
        }

        void PrintFrameText()
        {
            frameText.text = $"{VoxelAnimator.Instance.GetCurrentTimeStamp().ToString("F2")} / {VoxelAnimator.Instance.animationLenght.ToString("F2")}";
        }

        protected override int GetMaxValue()
        {
            return VoxelAnimator.Instance.GetFramesCount() - 1;
        }

        protected override void OnFrameChanged(int frameIndex)
        {
            slider.value = frameIndex;
        }

        protected override void OnValueChanged(int value)
        {
            ChangeFrame(value);
        }

        void ChangeFrame(int index)
        {
            VoxelAnimator.Instance.SetFrameBySliderValue(index);
            PrintFrameText();
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            VoxelAnimator.Instance.Pause();
            ChangeFrame((int)slider.value);
        }
    }
}
