using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ArtinessProject
{
    public class SpeedSlider : SliderInt
    {
        [SerializeField] int startStep = 2;
        [SerializeField] float[] speedSteps = new float[5] { 0.1f, 0.5f, 1f, 2f, 4f };

        TextMeshProUGUI speedText;

        protected override void Awake()
        {
            base.Awake();
            speedText = GetComponentInChildren<TextMeshProUGUI>();
        }

        protected override IEnumerator Start()
        {
            yield return base.Start();            
            slider.value = startStep;
        }

        protected override int GetMaxValue()
        {
            return speedSteps.Length - 1;
        }

        void ChangeSpeed(int index)
        {
            float speed = speedSteps[index];
            VoxelAnimator.Instance.SetAnimationSpeed(speed);
            speedText.text = $"x{speed.ToString("F1")}";
        }

        protected override void OnValueChanged(int value)
        {
            ChangeSpeed(value);
        }
    }
}
