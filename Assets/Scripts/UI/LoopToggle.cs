using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArtinessProject
{
    public class LoopToggle : UIElement
    {
        Toggle toggle;

        private void Awake()
        {
            toggle = GetComponent<Toggle>();
        }

        protected override IEnumerator Start()
        {
            yield return base.Start();
            toggle.isOn = VoxelAnimator.Instance.IsLoop();
        }

        public override void OnPointerDown(UnityEngine.EventSystems.PointerEventData eventData)
        {
            VoxelAnimator.Instance.SwitchIsLoop();
        }
    }
}
