using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ArtinessProject
{
    public class UIManager : Singleton<UIManager>
    {
        HashSet<UIElement> uiElementsOnInteraction = new HashSet<UIElement>();

        /// <summary>
        /// Querying this method is used to prevent incorrect interactions between UI and elements of the scene world,
        /// such as accidentally rotating an object while dragging a slider
        /// </summary>
        /// <returns></returns>
        public bool UserIsInteractingWithUI()
        {
            return uiElementsOnInteraction.Count > 0;
        }

        /// <summary>
        /// Querying this method is used to prevent incorrect interactions between UI and elements of the scene world,
        /// such as accidentally interact with an object while clicking a button
        /// </summary>
        /// <returns></returns>
        public bool UserIsInspectingUI()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }

        public void AddUIElementOnInteraction(UIElement uiElement)
        {
            uiElementsOnInteraction.Add(uiElement);
        }

        public void RemoveUIElementOnInteraction(UIElement uiElement)
        {
            uiElementsOnInteraction.Remove(uiElement);
        }

    }
}
