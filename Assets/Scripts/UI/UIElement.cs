using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ArtinessProject
{
    public class UIElement : MonoBehaviour, IPointerDownHandler
    {
        protected virtual IEnumerator Start()
        {
            yield return new WaitUntil(() => VoxelAnimator.Instance != null);
            VoxelAnimator.Instance.OnFrameChanged += OnFrameChanged;
            VoxelAnimator.Instance.OnStateChanged += OnStateChanged;

        }

        private void OnDestroy()
        {
            VoxelAnimator.Instance.OnFrameChanged -= OnFrameChanged;
            VoxelAnimator.Instance.OnStateChanged -= OnStateChanged;
        }

        protected virtual void OnFrameChanged(int frameIndex)
        {
            
        }

        protected virtual void OnStateChanged(VoxelAnimator.State newState)
        {
            
        }

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            
        }
    }
}