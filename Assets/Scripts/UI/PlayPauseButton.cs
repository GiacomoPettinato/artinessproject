using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArtinessProject
{
    public class PlayPauseButton : UIElement
    {
        [System.Serializable]
        public struct ButtonImage
        {
            public VoxelAnimator.State animatorState;
            public Sprite sprite;
        }

        [SerializeField] Image buttonImg;
        [SerializeField] ButtonImage[] images = new ButtonImage[2];

        protected override IEnumerator Start()
        {
            yield return base.Start();            
            buttonImg.sprite = GetSprite(VoxelAnimator.Instance.GetState());
        }

        protected override void OnStateChanged(VoxelAnimator.State newState)
        {
            buttonImg.sprite = GetSprite(newState);
        }

        public override void OnPointerDown(UnityEngine.EventSystems.PointerEventData eventData)
        {
            VoxelAnimator.Instance.SwitchState();
        }

        Sprite GetSprite(VoxelAnimator.State state)
        {
            foreach (ButtonImage image in images)
            {
                if (image.animatorState == state)
                {
                    return image.sprite;
                }
            }

            return null;
        }
    }
}