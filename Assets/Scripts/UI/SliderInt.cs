using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ArtinessProject
{
    public class SliderInt : UIElement, IBeginDragHandler, IEndDragHandler
    {
        protected Slider slider;

        protected virtual void Awake()
        {
            slider = GetComponent<Slider>();
            slider.onValueChanged.AddListener(delegate { OnValueChanged((int)slider.value); });
        }

        protected override IEnumerator Start()
        {
            yield return base.Start();
            InitSlider();
        }

        void InitSlider()
        {
            slider.wholeNumbers = true;
            slider.value = 0;
            slider.minValue = 0;
            slider.maxValue = GetMaxValue();
        }

        protected virtual int GetMaxValue()
        {
            return -1;
        }

        protected virtual void OnValueChanged(int value)
        {
           
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            UIManager.Instance.AddUIElementOnInteraction(this);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            UIManager.Instance.RemoveUIElementOnInteraction(this);
        }
    }
}