using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArtinessProject
{
    public class QuitApplication : MonoBehaviour
    {
        public void Quit()
        {
            Application.Quit();
        }
    }
}

