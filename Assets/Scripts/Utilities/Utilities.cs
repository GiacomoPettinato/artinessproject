using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ArtinessProject
{
    public static class Utilities
    {
#if UNITY_EDITOR
        public static T CreateScriptableObject<T>(string path, string name) where T : ScriptableObject
        {
            T scriptableObj = ScriptableObject.CreateInstance<T>();
            CreateAsset(scriptableObj, path, name);
            return scriptableObj;
        }

        public static T InstantiateObjectAsAsset<T>(T obj, string path, string name) where T : Object
        {
            T objAsset = Object.Instantiate(obj);
            CreateAsset(objAsset, path, name);
            return objAsset;
        }

        static void CreateAsset(Object obj, string path, string name)
        {
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath($"{path}/{name}.asset");
            AssetDatabase.CreateAsset(obj, assetPathAndName);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }


        public static string CreateFolder(string path, string name)
        {
            string guid = AssetDatabase.CreateFolder(path, name);
            return AssetDatabase.GUIDToAssetPath(guid);
        }
#endif
    }
}
