using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ArtinessProject
{
#if UNITY_EDITOR
    public class VoxelAnimationClipMaker : EditorWindow
    {
        VoxelAnimator voxelAnimator;
        VoxelMeshParameters meshParameters;
          
        string rawDataFolderPath = "Data/SampleRawFiles";
        string newClipFolderPath = "Data/VoxelAnimationClips";
        string newClipName = "Beating Hearth Clip";

        float labelWidth = 150;
        float fieldWidth = 200;

        /// <summary>
        /// Create new clip as ScriptableObject asset, which contains all references to meshes and raw data
        /// </summary>
        VoxelAnimationClip CreateClipFromRawData()
        {
            VoxelAnimationClip newClip = CreateClip();
            CreateFrames(newClip, GetRawDataFiles());

            //the tool allocates the created clip directly in animator, if assigned by user
            if (voxelAnimator != null)
            {
                voxelAnimator.SetClipToPlay(newClip);
            }

            return newClip;
        }

        /// <summary>
        /// Create a new clip and put it in one parent folder. Then, create one subfolder to contain all generated meshes 
        /// </summary>
        /// <returns></returns>
        VoxelAnimationClip CreateClip()
        {
            //The paths are assigned in tool fields
            string clipFolderPath = Utilities.CreateFolder($"Assets/{newClipFolderPath}", newClipName);
            string meshesClipFolderPath = Utilities.CreateFolder(clipFolderPath, $"{newClipName} Meshes");

            VoxelAnimationClip newClip = Utilities.CreateScriptableObject<VoxelAnimationClip>(clipFolderPath, newClipName);            
            newClip.meshesClipFolderPath = meshesClipFolderPath;

            return newClip;
        }

        /// <summary>
        /// Reorder and return all raw data files contained in folder as list of Objects
        /// </summary>       
        /// <returns></returns>
        List<Object> GetRawDataFiles()
        {
            List<Object> rawFiles = new List<Object>();
            
            DirectoryInfo info = new DirectoryInfo($"Assets/{rawDataFolderPath}");
            //see https://stackoverflow.com/questions/12077182/c-sharp-sort-files-by-natural-number-ordering-in-the-name
            var files = info.GetFiles("*.raw").OrderBy(n => Regex.Replace(n.Name, @"\d+", n => n.Value.PadLeft(4, '0')));

            foreach (FileInfo file in files)
            {
                rawFiles.Add(AssetDatabase.LoadAssetAtPath($"Assets/{rawDataFolderPath}/{file.Name}", typeof(Object)));
            }

            return rawFiles;
        }

        /// <summary>
        /// Create one Mesh for each <paramref name="rawDataFiles"/> and save it in the <paramref name="clip"/>
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="rawDataFiles"></param>
        void CreateFrames(VoxelAnimationClip clip, List<Object> rawDataFiles)
        {
            for (int i = 0; i < rawDataFiles.Count; i++)
            {
                Object rawData = rawDataFiles[i];

                //Ask to VoxelMeshGenerator to create a new mesh from raw data, using the parameters contained in the VoxelMeshParameters struct
                Mesh mesh = VoxelMeshGenerator.GenerateVoxelMeshFromRawData(rawData, meshParameters);

                //Instantiate and save the mesh as asset in the subfolder of the clip
                mesh = Utilities.InstantiateObjectAsAsset(mesh, clip.meshesClipFolderPath, $"{rawData.name} Mesh");

                //Create a new frame struct to save mesh references in the clip
                VoxelClipFrame frame = new VoxelClipFrame() { rawData = rawData, voxelMesh = mesh };
                clip.frames.Add(frame);
            }
        }

        [MenuItem("Tools/Voxel AnimationClip Maker")]
        static void Init()
        {
            VoxelAnimationClipMaker window = (VoxelAnimationClipMaker)GetWindow(typeof(VoxelAnimationClipMaker));
            window.Show();
            EditorStyles.textField.wordWrap = true;
        }

        private void OnEnable()
        {
            //fill the fields on tool enable, just to set a default reference for the user
            meshParameters = FindFirstScriptableObject<VoxelMeshParameters>();
            voxelAnimator = FindObjectOfType<VoxelAnimator>();
        }

        void OnGUI()
        {
            GUILayout.Space(20);

            //Show the tool title
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Voxel AnimationClip Maker", EditorStyles.boldLabel);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(20);

            //Create all text fields to assign clip path and name
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                GUILayout.BeginVertical("Box");

                //the folder where to get raw data files
                rawDataFolderPath = GetTextInField("RawData Path", rawDataFolderPath);
                //the path where to save the new clip and meshes
                newClipFolderPath = GetTextInField("New Clip Path", newClipFolderPath);
                newClipName = GetTextInField("New Clip Name", newClipName);

                GUILayout.EndVertical();
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();

            //Reference fields
            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                GUILayout.BeginVertical("Box");

                //the parameters to properly create the desidered mesh
                meshParameters = GetObjectInField("Voxel Mesh Parameters", meshParameters);
                //if assigned, once the clip is created it is directly placed into the animator
                voxelAnimator = GetObjectInField("Voxel Animator", voxelAnimator);

                GUILayout.EndVertical();
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                GUILayout.BeginVertical("Box");

                //from here, it starts the main function of the program
                if (GUILayout.Button("Create Clip!", GUILayout.Width(350), GUILayout.Height(30)))
                {
                    EditorUtility.SetDirty(CreateClipFromRawData());
                }

                GUILayout.EndVertical();
                GUILayout.FlexibleSpace();
            }
            GUILayout.EndHorizontal();

        }

        T FindFirstScriptableObject<T>() where T : ScriptableObject
        {
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
            T[] assets = new T[guids.Length];

            if (assets.Length > 0)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[0]);
                return AssetDatabase.LoadAssetAtPath<T>(path);
            }

            return null;
        }


        T GetObjectInField<T>(string label, T obj) where T : Object
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(label, GUILayout.Width(labelWidth));
            obj = EditorGUILayout.ObjectField(obj, typeof(T), true, GUILayout.Width(fieldWidth)) as T;
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            return obj;
        }

        string GetTextInField(string label, string text)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(label, GUILayout.Width(labelWidth));
            text = EditorGUILayout.TextField(text, GUILayout.Width(fieldWidth));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            return text;
        }


    }
#endif
}


