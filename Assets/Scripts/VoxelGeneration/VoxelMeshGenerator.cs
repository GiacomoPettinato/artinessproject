using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ArtinessProject
{  

#if UNITY_EDITOR

    /// <summary>
    /// This class creates a voxel mesh by decoding raw data at editor time
    /// </summary>
    public static class VoxelMeshGenerator
    {
        static Mesh mesh;      
        static List<Vector3> vertices = new List<Vector3>();
        static List<int> triangles = new List<int>();
        static HashSet<Vector3Int> voxelPositions = new HashSet<Vector3Int>();

        //these offsets are used during the cube face creation, to check if in that direction there is already a voxel cube (in that case, don't create the face)
        public static Vector3Int[] offsets =
        {
            Vector3Int.forward,
            Vector3Int.right,
            Vector3Int.back,
            Vector3Int.left,
            Vector3Int.up,
            Vector3Int.down
        };

        //store the coordinates to create the 8 vertices of a cube
        public static Vector3[] verticesCoord =
        {
            //the 4 vertices of "forward" face
            /*[0]*/new Vector3(1, 1, 1),
            /*[1]*/new Vector3(-1, 1, 1),
            /*[2]*/new Vector3(-1, -1, 1),
            /*[3]*/new Vector3(1, -1, 1),

            //the 4 vertices of "backward" face
            /*[4]*/new Vector3(-1, 1, -1),
            /*[5]*/new Vector3(1, 1, -1),
            /*[6]*/new Vector3(1, -1, -1),
            /*[7]*/new Vector3(-1, -1, -1)
        };

        //store the sets of integers which tells each face which verticesCoord index it needs to refer
        public static int[][] faceTrianglesSets =
        {
            new int[] { 0, 1, 2, 3 }, //forward            
            new int[] { 5, 0, 3, 6 }, //right           
            new int[] { 4, 5, 6, 7 }, //backward            
            new int[] { 1, 4, 7, 2 }, //left
            new int[] { 5, 4, 1, 0 }, //up
            new int[] { 3, 2, 7, 6 }  //down
        };

        /// <summary>
        /// Recreate a Texture3D encoded in <paramref name="rawData"/>. Then, convert pixels in world positions, where cubes will take place to make a voxel mesh  
        /// </summary>
        /// <param name="rawData"></param>
        /// <param name="meshParameters"></param>
        /// <returns></returns>
        public static Mesh GenerateVoxelMeshFromRawData(Object rawData, VoxelMeshParameters meshParameters)
        {
            voxelPositions.Clear();
            mesh = new Mesh();
         
            Texture3D texture3D = GetTexture3DFromRawData(rawData, meshParameters.meshBoundSize);
            SetVoxelPositionsFromTexture3DPixels(texture3D, meshParameters);
            CreateCubes(meshParameters.voxelScale);
            FinalizeMesh();

            return mesh;
        }

        /// <summary>
        /// Read all bytes of <paramref name="rawData"/> to make a <paramref name="size"/>*<paramref name="size"/>*<paramref name="size"/> Texture3D
        /// </summary>
        /// <param name="rawData"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        static Texture3D GetTexture3DFromRawData(Object rawData, int size)
        {
            //Alpha-only texture format, 8 bit integer (33Kb raw data dimension)
            Texture3D texture3D = new Texture3D(size, size, size, TextureFormat.Alpha8, false);
            texture3D.wrapMode = TextureWrapMode.Clamp;

            string path = AssetDatabase.GetAssetPath(rawData);
            byte[] bytes = File.ReadAllBytes(path);
            texture3D.SetPixelData(bytes, 0);

            return texture3D;
        }

        /// <summary>
        /// Cycle in the three dimensions to get the pixels of <paramref name="texture3D"/> and convert them into world positions
        /// </summary>
        /// <param name="texture3D"></param>
        /// <param name="parameters"></param>
        static void SetVoxelPositionsFromTexture3DPixels(Texture3D texture3D, VoxelMeshParameters parameters)
        {
            vertices.Clear();
            triangles.Clear();
           
            for (int x = 0; x < parameters.meshBoundSize; x++)
            {
                for (int y = 0; y < parameters.meshBoundSize; y++)
                {
                    for (int z = 0; z < parameters.meshBoundSize; z++)
                    {
                        Color color = texture3D.GetPixel(x, y, z);

                        //the threshold indicates which voxels should not be shown to the user. If condition is not met, ignore the pixel
                        //Only alpha channel is inspected, because the TextureFormat is Alpha8, alpha-only format
                        if (color.a >= parameters.threshold)
                        {
                            //the HashSet<Vector3Int> is populated here, before voxel making. 
                            //This was done not simultaneously with the creation of the cubes to have additional control during the creation of the faces.
                            //In fact, if some cubes are adjacent, the neighboring faces are not created, in order to avoid wasting resources.
                            voxelPositions.Add(new Vector3Int(x, y, z) + parameters.meshPivotOffset);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create a voxel cube for each position of voxelPositions.
        /// </summary>
        /// <param name="scale"></param>
        static void CreateCubes(float scale)
        {
            float adjScale = scale * 0.5f;

            foreach (Vector3Int pos in voxelPositions)
            {
                CreateCube(pos, adjScale);
            }
        }
        
        /// <summary>
        /// Create voxel cube in <paramref name="cubePos"/>, making its vertices, triangles and faces
        /// </summary>
        /// <param name="cubePos"></param>
        /// <param name="cubeScale"></param>
        static void CreateCube(Vector3Int cubePos, float cubeScale)
        {
            //create the six faces of the cube making their vertices and triangles
            for (int i = 0; i < 6; i++)
            {
                //Interrogate voxelPositions to find out if there is a neighboring cube in the direction of the face ready to be made.
                //If the HashSet returns a position, it means there is a cube there, so skip the face making
                if (voxelPositions.Contains(offsets[i] + cubePos)) 
                    continue;                

                //loop for each direction using the index
                CreateQuadVertices(cubePos, i, cubeScale);
                CreateQuadTriangles();
                
            }
        }

        /// <summary>
        /// Create the four vertices of a quad towards a direction. The integer <paramref name="dir"/> is used as index of faceTrianglesSets to point in a direction
        /// </summary>
        /// <param name="cubePos"></param>
        /// <param name="dir"></param>
        /// <param name="faceScale"></param>
        static void CreateQuadVertices(Vector3Int cubePos, int dir, float faceScale)
        {
            //the four vertices of the new face
            Vector3[] newVertices = new Vector3[4];

            for (int i = 0; i < newVertices.Length; i++)
            {
                newVertices[i] = (verticesCoord[faceTrianglesSets[dir][i]] * faceScale) + cubePos;
            }

            vertices.AddRange(newVertices);
        }

        /// <summary>
        /// Create the two triangles of a quad 
        /// </summary>
        static void CreateQuadTriangles()
        {
            //start from the first vertice made in the new face
            int count = vertices.Count - 4;

            //first triangle
            triangles.Add(count);
            triangles.Add(count + 1);
            triangles.Add(count + 2);

            //second triangle
            triangles.Add(count);
            triangles.Add(count + 2);
            triangles.Add(count + 3);
        }

        /// <summary>
        /// Create the mesh using all stored vertices and triangles
        /// </summary>
        static void FinalizeMesh()
        {
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.RecalculateNormals();
            mesh.Optimize();
        }
    }
#endif
}
