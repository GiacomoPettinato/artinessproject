using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArtinessProject
{
    [CreateAssetMenu(menuName = "Voxel Mesh Parameters")]
    public class VoxelMeshParameters : ScriptableObject
    {
        public int meshBoundSize = 32;
        public float voxelScale = 1;
        public float threshold = 0.4f;
        public Vector3Int meshPivotOffset = new Vector3Int(-16, -16, -16);
    }
}
