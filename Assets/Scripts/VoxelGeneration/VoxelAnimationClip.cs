using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArtinessProject
{
    [System.Serializable] public struct VoxelClipFrame
    {
        public Mesh voxelMesh;
        public Object rawData;
    }

    [CreateAssetMenu(menuName = "Voxel Animation Clip")]
    public class VoxelAnimationClip : ScriptableObject
    {
        public float animationLength = 1.0f; 
        [ReadOnly] public string meshesClipFolderPath;
        public List<VoxelClipFrame> frames = new List<VoxelClipFrame>();
        
    }

}

