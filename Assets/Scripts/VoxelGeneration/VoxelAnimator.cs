using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArtinessProject
{
    public class VoxelAnimator : Singleton<VoxelAnimator>
    {
        [Tooltip("You can create a clip using the Voxel AnimationClip Maker tool (Tools/Voxel AnimationClip Maker)"), SerializeField]    
        VoxelAnimationClip clipToPlay;

        [SerializeField] MeshFilter meshFilter;

        public enum State { Pause, Play }

        //Subscriber/Publisher pattern events, used mostly by classes inheriting from UIElement
        public delegate void FrameChanged(int frameIndex);
        public event FrameChanged OnFrameChanged;

        public delegate void StateChanged(State newState);
        public event StateChanged OnStateChanged;

        

        [SerializeField] State state;
        [SerializeField] bool loop;
        [SerializeField] float animationSpeed = 1.0f;
        public float timeStamp { get; private set; }
        public int currentFrameIndex { get; private set; }
        public float animationLenght { get; private set; }
      
        float elapsedTime = 0.0f;

        protected override void Awake()
        {
            base.Awake();

            animationLenght = clipToPlay.animationLength;

            //Calculate the duration in seconds of a single frame
            timeStamp = Mathf.InverseLerp(0.0f, GetFramesCount(), animationLenght);
            SetMeshByFrame(0);
        }

        public float GetCurrentTimeStamp()
        {
            return (currentFrameIndex + 1) * timeStamp;
        }

        public int GetFramesCount()
        {
            return clipToPlay.frames.Count;
        }

        /// <summary>
        /// If true, the animation loops and it doesn't stop until user input
        /// </summary>
        /// <returns></returns>
        public bool IsLoop()
        {
            return loop;
        }

        public void SetAnimationSpeed(float speed)
        {
            animationSpeed = speed;
        }

        public State GetState()
        {
            return state;
        }

        /// <summary>
        /// This method is called by VoxelAnimationClipMaker tool, once <paramref name="clip"/> has been made
        /// </summary>
        /// <param name="clip"></param>
        public void SetClipToPlay(VoxelAnimationClip clip)
        {
            clipToPlay = clip;
        }

        private void Update()
        {
            if (state == State.Play)
            {
                Play();
            }
        }

        public void Pause()
        {
            SetState(State.Pause);
        }

        /// <summary>
        /// Retrieve the already made mesh in the frames stored in the clip
        /// </summary>
        /// <param name="frameIndex"></param>
        void SetMeshByFrame(int frameIndex)
        {
            meshFilter.mesh = clipToPlay.frames[frameIndex].voxelMesh;
        }

        public void SetFrameBySliderValue(int frameIndex)
        {
            if (state != State.Pause)
                return;

            currentFrameIndex = frameIndex;
            SetMeshByFrame(currentFrameIndex);
        }

        void SetState(State newState)
        {
            if (newState == state)
                return;

            elapsedTime = 0.0f;
            state = newState;
            OnStateChanged?.Invoke(newState);
        }

        void Play()
        {
            elapsedTime += Time.deltaTime * animationSpeed;

            if (elapsedTime >= timeStamp)
            {
                ChangeFrame();
            }
        }

        public void SwitchState()
        {
            SetState(1 - state);
        }

        public void SwitchIsLoop()
        {
            loop = !loop;
        }

        void ChangeFrame()
        {
            elapsedTime = 0.0f;

            bool isLastFrame = currentFrameIndex == GetFramesCount() - 1;
            currentFrameIndex = isLastFrame ? 0 : currentFrameIndex + 1;

            if (isLastFrame && !loop)
            {
                Pause();
            }

            SetMeshByFrame(currentFrameIndex);
            OnFrameChanged?.Invoke(currentFrameIndex);

        }
    }
}